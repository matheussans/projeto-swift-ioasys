//
//  Empresas.swift
//  projeto
//
//  Created by Matheus Sans on 24/01/20.
//  Copyright © 2020 SansBusiness. All rights reserved.
//

import UIKit

class Empresas {
    var nome: String
    var categoria: String
    var nacionalidade: String
    var descricao: String
    var imagem: UIImage
    
    init(nome: String, categoria: String, nacionalidade: String, descricao: String){
        self.nome = nome.capitalized
        self.categoria = categoria.capitalized
        self.nacionalidade = nacionalidade.capitalized
        self.descricao = descricao.capitalized
        
        imagem = UIImage(named: "empresa")!
    }
}

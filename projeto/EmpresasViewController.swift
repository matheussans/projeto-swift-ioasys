//
//  EmpresasViewController.swift
//  projeto
//
//  Created by Matheus Sans on 24/01/20.
//  Copyright © 2020 SansBusiness. All rights reserved.
//

import UIKit

class EmpresasViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var descricaoLbl: UILabel!
    
    var empresa:Empresas?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        descricaoLbl.text = empresa?.descricao
        
        imageView.image = empresa?.imagem
        
        // Do any additional setup after loading the view.
    }


}
